from django.contrib import admin
from .models import Sale, SalesPerson, Customer

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass


@admin.register(SalesPerson)
class SalesPeopleAdmin(admin.ModelAdmin):
    pass


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass
