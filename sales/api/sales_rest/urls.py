from django.urls import path
from .views import api_list_salesperson, api_salesperson_detail, api_list_customer, api_customer_detail, api_list_sale, api_sale_detail

urlpatterns = [
    path('salespeople/', api_list_salesperson, name="api_list_salesperson"),
    path('salespeople/<int:id>/', api_salesperson_detail, name="api_salesperson_detail"),
    path('customers/', api_list_customer, name="api_list_customer"),
    path('customers/<int:id>/', api_customer_detail, name="api_customer_detail"),
    path('sales/', api_list_sale, name="api_list_sale"),
    path('sales/<int:id>/', api_sale_detail, name="api_sale_detail"),
]
