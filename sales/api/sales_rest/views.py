from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import SalesPerson, Customer, AutomobileVO, Sale
from .encoders import SalesPersonEncoder, CustomerEncoder, AutomobileVOEncoder, SaleEncoder

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salespeople": salesperson},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_salesperson_detail(request, id):
    if request.method == "GET":
        try:
            salesperson = SalesPerson.objects.get(id=id)
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalesPersonEncoder,
                safe=False
                )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "SalesPerson does not exist"}, status=400)
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.get(id=id).update(**content)
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalesPersonEncoder,
                safe=False
                )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "SalesPerson does not exist"}, status=400)
            return response
    else:
        try:
            count, _ = SalesPerson.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0}, safe=False)
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "SalesPerson does not exist"}, status=400)
            return response

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"}, status=400)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer_detail(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False
                )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"}, status=400)
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id).update(**content)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"}, status=400)
            return response
    else:
        try:
            count, _ = Customer.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0}, safe=False)
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"}, status=400)
            return response


@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            id = content["salesperson"]
            salesperson = SalesPerson.objects.get(id=id)
            content["salesperson"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson"},
                status=400
            )

        try:
            customer = content["customer"]
            customer = Customer.objects.get(id=customer)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer"},
                status=400
            )

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile VIN"},
                status=400
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sale_detail(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                {"sale": sale},
                encoder=SaleEncoder,
                safe=False
                )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"}, status=400)
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(id=id).update(**content)
            return JsonResponse(
                {"sale": sale},
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"}, status=400)
            return response
    else:
        try:
            count, _ = Sale.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0}, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale does not exist"}, status=400)
            return response
