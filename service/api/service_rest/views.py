from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model= AutomobileVO
    properties=[
        "vin",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties=[
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties=[
        "vin",
        "date_time",
        "technician",
        "reason",
        "status",
        "customer",
        "id"
    ]
    encoders={"technician":TechnicianEncoder()}
    def get_extra_data(self, o):
        count= AutomobileVO.objects.filter(vin=o.vin).count()
        return {"vip": count>0}

@require_http_methods(["GET", 'POST'])
def api_list_technicians(request):
    if request.method=="GET":
        technician =  Technician.objects.all()
        return JsonResponse(
            {"technician":technician},
            encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )



@require_http_methods(['DELETE'])
def api_delete_technician(request,id):
    count , _ = Technician.objects.get(employee_id=id).delete()
    return JsonResponse({"deleted": count > 0}, status=200)


@require_http_methods(["GET", 'POST'])
def api_list_appointments(request):
    if request.method =="GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            tech_id= content['technician']
            tech=Technician.objects.get(employee_id=tech_id)
            content['technician']=tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message":"Wrong technician ID"},
                status=400
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["GET"])
def api_appointments_vin(request, vin):

    if not AutomobileVO.objects.filter(vin=vin).exists():
        return JsonResponse({"message": "That vin number doesnt exist my dude!"}, status=404)
    vin= Appointment.objects.filter(vin=vin)
    return JsonResponse(
        {"vin":vin},
        encoder=AppointmentEncoder,
        safe=False
    )


@require_http_methods(['DELETE'])
def api_delete_appointment(request, id):
    count , _ = Appointment.objects.get(id=id).delete()
    return JsonResponse({"deleted": count > 0}, status=200)


@require_http_methods(['PUT'])
def api_finish_appointment(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(['PUT'])
def api_cancel_appointment(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


# Create your views here.
