from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    year = models.CharField(max_length=4)
    color = models.CharField(max_length=50)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin



class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.AutoField(primary_key=True)


class Appointment(models.Model):
    customer = models.CharField(max_length=200)
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=50)
    vin = models.CharField(max_length=17)

    technician = models.ForeignKey(
        Technician,
        related_name= "appointments",
        on_delete=models.CASCADE,
    )

class Manufacturer(models.Model):
    Manufacturer = models.CharField(max_length=150)

# Create your models here.
