import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const VehicleModelForm = () => {
	const [name, setName] = useState('');
	const [pictureUrl, setPictureUrl] = useState('');
	const [manufacturerId, setManufacturerId] = useState('');
	const [manufacturers, setManufacturers] = useState([]);
	const navigate = useNavigate();
	const [successAlert, setSuccessAlert] = useState(false);

	useEffect(() => {
		const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
		fetch(manufacturersUrl)
			.then((response) => response.json())
			.then((data) => setManufacturers(data.manufacturers));
	}, []);

	const handleSubmit = (event) => {
		event.preventDefault();
		const newVehicleModel = {
			name: name,
			picture_url: pictureUrl,
			manufacturer_id: manufacturerId,
		};

		const modelUrl = 'http://localhost:8100/api/models/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(newVehicleModel),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		fetch(modelUrl, fetchConfig)
			.then((response) => response.json())
			.then(() => {
				setSuccessAlert(true);
				const alertTimeout = setTimeout(() => {
					setSuccessAlert(false);
				}, 3000);
				const navigateTime = setTimeout(() => {
					navigate('/models');
				}, 1000);
				return () => {
					clearTimeout(alertTimeout);
					clearTimeout(navigateTime);
				};
			});
	};

	const handleNameChange = (event) => {
		const value = event.target.value;
		setName(value);
	};
	const handlePictureUrlChange = (event) => {
		const value = event.target.value;
		setPictureUrl(value);
	};
	const handleManufacturerIdChange = (event) => {
		const value = event.target.value;
		setManufacturerId(value);
	};
	return (
		<div>

			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Vehicle model added!
				</div>
			)}
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1>Create a new Vehicle Model</h1>
						<form onSubmit={handleSubmit} id="create-Vehicle-Model-form">
							<div className="form-floating mb-3">
								<input
									value={name}
									onChange={handleNameChange}
									placeholder="Name"
									required
									type="text"
									name="Name"
									id="Name"
									className="form-control placeholder"
								/>
								<label htmlFor="Name">Name</label>
							</div>

							<div className="form-floating mb-3">
								<input
									value={pictureUrl}
									onChange={handlePictureUrlChange}
									placeholder="pictureUrl"
									required
									type="url"
									name="pictureUrl"
									id="pictureUrl"
									className="form-control placeholder"
								/>
								<label htmlFor="picture url">picture Url</label>
							</div>

							<div className="mb-3">
								<select
									value={manufacturerId}
									onChange={handleManufacturerIdChange}
									required
									name="manufacturer"
									id="manufacturer"
									className="form-select place"
								>
									<option value="">Choose a manufacturer</option>
									{manufacturers.map((manufacturerId) => {
										return (
											<option key={manufacturerId.id} value={manufacturerId.id}>
												{manufacturerId.name}
											</option>
										);
									})}
								</select>
							</div>

							<button className="btn btn-primary">Create</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};
export default VehicleModelForm;
