import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const AppointmentForm = () => {
	const [vin, setVin] = useState('');
	const [customer, setCustomer] = useState('');
	const [dateTime, setDateTime] = useState([]);
	const [technician, setTechnician] = useState('');
	const [technicians, setTechnicians] = useState([]);
	const [reason, setReason] = useState('');
	const navigate = useNavigate();
	const [successAlert, setSuccessAlert] = useState(false);
	const [isVip, setIsVip] = useState(false);


	useEffect(() => {
		const techUrl = 'http://localhost:8080/api/technicians/';
		fetch(techUrl)
			.then((response) => response.json())
			.then((data) => setTechnicians(data.technician));
	}, []);

	const handleSubmit = (event) => {
		event.preventDefault();
		const newAppt = {
		  date_time: dateTime,
		  reason: reason,
		  status: 'created',
		  vin: vin,
		  customer: customer,
		  technician: technician,
		};

		fetch('http://localhost:8080/api/appointments/', {
		  method: 'POST',
		  headers: { 'Content-Type': 'application/json' },
		  body: JSON.stringify(newAppt),
		})
		.then(response => {
		  if (!response.ok) {
			throw new Error('Network response was not ok');
		  }
		  return response.json();
		})
		.then(() => {
		  setSuccessAlert(true);
		  setTimeout(() => navigate('/appointments'), 3000); // Wait a bit before navigating
		})
		.catch(error => {
		  console.error('Error creating appointment:', error);
		  // Consider setting an error state here to notify the user
		});
	  };


	const handleVinChange = (event) => {
		setVin(event.target.value);
	};

	const handleCustomerChange = (event) => {
		setCustomer(event.target.value);
	};

	const handleDateTimeChange = (event) => {
		setDateTime(event.target.value);
	};

	const handleTechnicianChange = (event) => {
		setTechnician(event.target.value);
	};

	const handleReasonChange = (event) => {
		setReason(event.target.value);
	};

	const newAppt = {
		// Other appointment details
		vip: isVip ? "Yes" : "No", // Convert boolean to "Yes" or "No"
	  };
	return (
		<div>
			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Service appointment created!
				</div>
			)}
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1>Create a Service Appointment</h1>
						<form onSubmit={handleSubmit} id="create-service-appointment-form">
							<div className="form-floating mb-3">
								<input
									value={vin}
									onChange={handleVinChange}
									placeholder="VIN"
									required
									type="text"
									name="VIN"
									id="VIN"

									maxLength={17}
									className="form-control placeholder"
									style={{ textTransform: 'uppercase' }}
								/>
								<label htmlFor="VIN">Automobile VIN #</label>
							</div>

							<div className="form-floating mb-3">
								<input
									value={customer}
									onChange={handleCustomerChange}
									placeholder="Customer"
									required
									type="text"
									name="Customer"
									id="Customer"
									className="form-control placeholder"
								/>
								<label htmlFor="Customer">Customer</label>
							</div>
							<div className="form-check mb-3">
							<input
								className="form-check-input"
								type="checkbox"
								id="vipCheckbox"
								checked={isVip}
								onChange={(e) => setIsVip(e.target.checked)}
							/>
							<label className="form-check-label" htmlFor="vipCheckbox">
								VIP Customer
							</label>
							</div>
								<p>VIP Status: {isVip ? "Yes" : "No"}</p>

							<div className="form-floating mb-3">
								<input
									value={dateTime}
									onChange={handleDateTimeChange}
									placeholder="Date Time"
									required
									type="datetime-local"
									name="Date Time"
									id="Date Time"
									className="form-control placeholder"
								/>
								<label htmlFor="Date Time">Date/Time</label>
							</div>

							<div className="form-floating mb-3">
								<input
									value={reason}
									onChange={handleReasonChange}
									placeholder="Reason"
									required
									type="text"
									name="Reason"
									id="Reason"
									className="form-control placeholder"
								/>
								<label htmlFor="Reason">Reason for visit</label>
							</div>

							<div className="mb-3">
								<select
									value={technician}
									onChange={handleTechnicianChange}
									required
									name="Technicians"
									id="Technicians"
									className="form-select"
								>
									<option value="">Technicians</option>
									{technicians.map((technician) => {
										return (
											<option
												key={technician.employee_id}
												value={technician.employee_id}
											>
												{technician.first_name} {technician.last_name}
											</option>
										);
									})}
								</select>
							</div>

							<button className="btn btn-primary">Create</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};
export default AppointmentForm;
