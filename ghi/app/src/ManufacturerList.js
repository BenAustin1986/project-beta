import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import './Manufacturer.css';


function Manufacturer({ addManufacturer}){

    const [manufacturers, setManufacturers] = useState ([]);
    const [newManufacturerName, setNewManufacturerName] = useState('');



    const manufacturerDelete = async (manufacturer) => {
        const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturer.id}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(manufacturerUrl, fetchConfig);
        if(response.ok) {
            fetchManufacturer();
        }
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        // Assuming addManufacturer is a function that adds a manufacturer
        // You might need to adjust it based on how you're managing state or fetching data
        addManufacturer(newManufacturerName);
        setNewManufacturerName(''); // Reset input field
      };
    const fetchManufacturer = async () => {
        const url = "http://localhost:8100/api/manufacturers/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect (() => {
        fetchManufacturer();
    }, []);



    return (
        <>
          <div className="d-grid gap-2 d-sm-flex justify-content-between mt-2 mb-2">
            <Link to="/manufacturers/create" className="btn btn-primary btn-lg px-4 gap-3">Add a Manufacturer</Link>
            {/* <form onSubmit={handleSubmit} className="d-flex">
              <input
                type="text"
                className="form-control"
                placeholder="Manufacturer name"
                value={newManufacturerName}
                onChange={(e) => setNewManufacturerName(e.target.value)}
                required
              />
              <button type="submit" className="btn btn-success ms-2">Add</button>
            </form> */}
          </div>
          <table className="table table-striped" style={{ color: 'white' }}>
            <thead>
              <tr>
                <th>Name</th>
                <th>ID</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {manufacturers.map((manufacturer) => (
                <tr key={manufacturer.id}>
                  <td style={{ color: 'white' }}>{manufacturer.name}</td>
                  <td style={{ color: 'white' }}>{manufacturer.id}</td>

                  <td>
                    <button onClick={() => manufacturerDelete(manufacturer)} className="btn btn-danger">Delete</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      );
    }

    export default Manufacturer;
