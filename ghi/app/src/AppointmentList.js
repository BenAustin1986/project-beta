import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        fetchAppointments();
    }, []);

    const fetchAppointments = async () => {
        const fetchURL = 'http://localhost:8080/api/appointments/';
        const response = await fetch(fetchURL);
        if (response.ok) {
            const data = await response.json();
            const appointmentsData = data.appointment.map(appointment => {
                return {
                    ...appointment,
                };
            });
            setAppointments(appointmentsData);
        } else {
            console.error('Failed to fetch appointments:', response.statusText);
        }
    };

    const handleCancel = async (id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const fetchConfig = {
            method: 'put',
        };
        const response = await fetch(cancelUrl, fetchConfig);
        if (response.ok) {
            setAppointments(appointments.filter((appointment) => appointment.id !== id));
            navigate('/appointments');
        }
    };

    const handleFinish = async (id) => {
        const finishUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
        const fetchConfig = {
            method: 'put',
        };
        const response = await fetch(finishUrl, fetchConfig);
        if (response.ok) {
            setAppointments(appointments.filter((appointment) => appointment.id !== id));
            navigate('/appointments');
        }
    };




    return (
        <>
            <h1 style={{ margin: '25px 0' }}>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date/Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>
                                {new Date(appointment.date_time).toLocaleString('en-US', {
                                    month: '2-digit',
                                    day: '2-digit',
                                    year: '2-digit',
                                    hour: 'numeric',
                                    minute: 'numeric',
                                    hour12: true,
                                })}
                            </td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <button onClick={() => handleCancel(appointment.id)} type="button" className="btn btn-outline-danger">Cancel</button>
                                <button onClick={() => handleFinish(appointment.id)} type="button" className="btn btn-success">Finish</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
}

export default AppointmentList;
