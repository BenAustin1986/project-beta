import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function TechniciansList() {
	const [technician, setTechnician] = useState([]);
	const navigate = useNavigate();

	useEffect(() => {
		const url = 'http://localhost:8080/api/technicians/';
		fetch(url)
			.then((response) => response.json())
			.then((data) => setTechnician(data.technician));
	}, []);

	return (
		<>
			<h1>Technicians</h1>
			<a href="/technicians/create">

				<button>Add Technician</button>
			</a>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Employee ID</th>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tbody>
					{technician.map((tech) => {
						return (
							<tr key={tech.employee_id}>
								<td>{tech.employee_id}</td>
								<td>{tech.first_name}</td>
								<td>{tech.last_name}</td>
								
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}
export default TechniciansList;
