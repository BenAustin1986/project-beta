import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function ManufacturerForm() {
	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [successAlert, setSuccessAlert] = useState(false);

	function handleNameChange(e) {
		const value = e.target.value;
		setName(value);
	}

	const handleSubmit = async (e) => {
		e.preventDefault();
		const data = {
			name: name,
		};

		const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		fetch(manufacturerUrl, fetchConfig)
			.then((response) => response.json())
			.then(() => {
				setSuccessAlert(true);
				const alertTimeout = setTimeout(() => {
					setSuccessAlert(false);
				}, 3000);
				const navigateTime = setTimeout(() => {
					navigate('/manufacturers');
				}, 1000);
				return () => {
					clearTimeout(alertTimeout);
				};
			});
	};

	return (
		<div>
			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Manufacturer added!
				</div>
			)}
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1 style={{color: 'white'}}>Create a new manufacturer</h1>
						<form onSubmit={handleSubmit} id="create-manufacturer-form">
							<div className="form-floating mb-3">
								<input
									onChange={handleNameChange}
									value={name}
									placeholder="name"
									required
									type="text"
									id="name"
									className="form-control placeholder"
								/>
								<label htmlFor="name">Name</label>
							</div>
							<button className="btn btn-primary">Create</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
}

export default ManufacturerForm;
