import React from 'react';
import { NavLink } from 'react-router-dom';
import './Footer.css'; // Make sure to create this CSS file for styling

const Footer = () => {
  return (
    <footer className="navbar navbar-expand-lg navbar-dark bg- glass-effect footer-fixed-bottom">
      <div className="container">
        <div className="row">
          <div className="col-16 col-md-4 text-center text-md-start">
            <NavLink to="/" className="text-white text-decoration-none">
              <strong>CarCar</strong>
            </NavLink>
          </div>
          <div className="col-12 col-md-4 text-center my-3 my-md-0">
            © {new Date().getFullYear()} CarCar, Inc.
          </div>
          <div className="col-12 col-md-4 text-center text-md-end">
            <NavLink to="/contact" className="text-white pe-5">Contact</NavLink>
            <NavLink to="/about" className="text-white pe-5">About</NavLink>
            <NavLink to="/privacy" className="text-white pe-5">Privacy</NavLink>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
