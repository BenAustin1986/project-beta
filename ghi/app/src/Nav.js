import { NavLink } from 'react-router-dom';


function Nav() {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg- glass-effect">
			<div className="container-fluid">
				<NavLink className="navbar-brand" to="/">
					CarCar
				</NavLink>
				<button
					className="navbar-toggler"
					type="button"
					aria-controls="navbarSupportedContent"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item">
							<NavLink className="nav-link" to="/">
								Home
							</NavLink>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								id="navbarDropdownManufacturers"
								role="button"
								href=""
								data-bs-toggle="dropdown"
							>
								Manufacturers
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownManufacturers"
							>
								<li>
									<NavLink className="dropdown-item" to="/manufacturers" end>
										List of Manufacturers
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/manufacturers/create">
										Add Manufacturer
									</NavLink>
								</li>
							</ul>
						</li>

						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownVehicleModels"
								data-bs-toggle="dropdown"
							>
								Vehicle Models
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownVehicleModels"
							>
								<li>
									<NavLink className="dropdown-item" to="/models" end>
										List of Models
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/models/create">
										Add a Model
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownAutomobiles"
								data-bs-toggle="dropdown"
							>
								Automobiles
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownAutomobiles"
							>
								<li>
									<NavLink className="dropdown-item" to="/automobiles" end>
										List of Automobiles
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/automobiles/create">
										Add Automobile
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownSalesPeople"
								data-bs-toggle="dropdown"
							>
								Sales People
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownSalesPeople"
							>
								<li>
									<NavLink className="dropdown-item" to="/salesperson" end>
										List of Sales People
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/salesperson/new">
										Add a Sales Person
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/salespersonhistory">
										Salesperson History
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownCustomers"
								data-bs-toggle="dropdown"
							>
								Customers
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownCustomers"
							>
								<li>
									<NavLink className="dropdown-item" to="/customers" end>
										List of Customers
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/customers/new">
										Create a Customer
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownSales"
								data-bs-toggle="dropdown"
							>
								Sales
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownSales"
							>
								<li>
									<NavLink className="dropdown-item" to="/sale" end>
										List of Sales
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/sale/new">
										Create a Sale
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownTechnicians"
								data-bs-toggle="dropdown"
							>
								Technicians
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownTechnicians"
							>
								<li>
									<NavLink className="dropdown-item" to="/technicians" end>
										List of Technicians
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/technicians/create">
										Add a Technician
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<a
								className="nav-link dropdown-toggle"
								role="button"
								href=""
								id="navbarDropdownAppointments"
								data-bs-toggle="dropdown"
							>
								Service Appointments
							</a>
							<ul
								className="dropdown-menu"
								aria-labelledby="navbarDropdownAppointments"
							>
								<li>
									<NavLink className="dropdown-item" to="/appointments" end>
										List of Appointments
									</NavLink>
								</li>
								<li>
									<NavLink
										className="dropdown-item"
										to="/appointments/create"
										end
									>
										Create a Service Appointment
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/appointments/history">
										Service History
									</NavLink>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Nav;
