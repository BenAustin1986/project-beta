import React, { useState, useEffect} from 'react';

function SalespersonHistory () {
    const [sales, setSales] = useState([]);
    const [search, setSearch] = useState('');

    const getData = async () => {
        const request = await fetch('http://localhost:8090/api/sales/');
        if (request.ok) {
            const resp = await request.json();
            setSales(resp.sales);
        } else {
            console.error("Request Error");
        }
    };

    const [salespeople, setSalesPeoples] = useState([]);
    const fetchSalesPeople = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalesPeoples(data.salespeople);
        }
    };

    useEffect(() => {
        getData();
        fetchSalesPeople();
    }, []);

    return (
        <>
            <div className='searchTerm'>
                <label className='search'>Employee</label>
                <select onChange={(event) => setSearch(event.target.value)}>
                    <option className='employee_id'>Choose Salesperson</option>
                    { salespeople.map(salesperson => {
                        return(
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <div>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.filter((sale) => {
                            return search === ''
                            ? sale
                            : String(sale.salesperson.employee_id).includes(search);
                        }).length === 0 ? <tr><td colSpan="4">No sales yet!</td></tr> : sales.filter((sale) => {
                            return search === ''
                            ? sale
                            : String(sale.salesperson.employee_id).includes(search);
                        }).map(sale => {
                            return (
                                <tr key={sale.id} value={sale.id}>
                                    <td>
                                        {sale.salesperson.first_name} {sale.salesperson.last_name}
                                    </td>
                                    <td>
                                        {sale.customer.first_name} {sale.customer.last_name}
                                    </td>
                                    <td>
                                        {sale.automobile.vin}
                                    </td>
                                    <td>
                                        ${sale.price}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default SalespersonHistory;
