import React, { useState, useEffect } from "react";

function SalesPersonList () {
    const [salesperson, setSalesperson] = useState([]);

    async function loadSalesperson() {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    }

    useEffect(() => {
        loadSalesperson();
    }, []);

    return (
        <>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson.map((salesperson) => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                                <td>{salesperson.employee_id}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SalesPersonList;
