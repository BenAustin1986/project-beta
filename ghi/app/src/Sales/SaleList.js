import React, { useState, useEffect } from "react";

function SaleList () {
    const [sales, setSales] = useState([]);

    async function loadSales() {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setSales(data.sales);

        }
    }

    useEffect(() => {
        loadSales();
    }, []);

    return (
        <>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Price</th>
                        <th>VIN</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => {
                        return (
                            <tr key={sale.employee_id}>
                                <td>{sale.salesperson.id}</td>
                                <td>
                                    {sale.salesperson.first_name} {sale.salesperson.last_name}
                                </td>
                                <td>
                                    {sale.customer.first_name} {sale.customer.last_name}
                                </td>
                                <td>
                                    ${sale.price}
                                </td>
                                <td>
                                    {sale.automobile.vin}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SaleList;
