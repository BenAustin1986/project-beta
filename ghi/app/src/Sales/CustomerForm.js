import React, { useState, useEffect } from "react";

function CustomerForm() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [successMessage, setSuccessMessage] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const customer = await fetch(customerUrl, fetchConfig);
        if (customer.ok) {
            setSuccessMessage('Customer created!');
            setFirstName("");
            setLastName("");
            setAddress("");
            setPhoneNumber("");
        }
    };

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    };

    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value);
    };

    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    };

    const handlePhoneNumberChange = (e) => {
        const value = e.target.value;
        setPhoneNumber(value);
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    {successMessage && <p style={{ backgroundColor: 'red', color: 'white', padding: '10px', borderRadius: '5px', display: 'inline-block' }}>{successMessage}</p>}
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floatin mb-3">
                            <input
                                value={firstName}
                                onChange={handleFirstNameChange}
                                placeholder="First Name" required type="text"
                                name="first_name" id="first_name"
                                className="form-control"
                            />
                            <label>First Name</label>
                        </div>
                        <div className="form-floatin mb-3">
                            <input
                                value={lastName}
                                onChange={handleLastNameChange}
                                placeholder="Last Name" required type="text"
                                name="last_name" id="last_name"
                                className="form-control"
                            />
                            <label>Last Name</label>
                        </div>
                        <div className="form-floatin mb-3">
                            <input
                                value={address}
                                onChange={handleAddressChange}
                                placeholder="Address" required type="text"
                                name="text" id="text"
                                className="form-control"
                            />
                            <label>Address</label>
                        </div>
                        <div className="form-floatin mb-3">
                            <input
                                value={phoneNumber}
                                onChange={handlePhoneNumberChange}
                                placeholder="Phone Number" required type="number"
                                name="text" id="text"
                                className="form-control"
                            />
                            <label>Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm;
