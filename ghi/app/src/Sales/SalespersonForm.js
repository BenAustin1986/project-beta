import React, { useState } from "react";

function SalesPersonForm({onSalesPersonAdded}) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");
  const [successMessage, setSuccessMessage] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const salesPersonUrl = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesPersonUrl, fetchConfig);
    if (response.ok) {
        setSuccessMessage('Salesperson created!');
        setFirstName("");
        setLastName("");
        setEmployeeId("");
    }
  };

  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };

  const handleEmployeeIdChange = (e) => {
    const value = e.target.value;
    setEmployeeId(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          {successMessage && <p style={{ backgroundColor: 'red', color: 'white', padding: '10px', borderRadius: '5px', display: 'inline-block' }}>{successMessage}</p>}
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                value={firstName}
                onChange={handleFirstNameChange}
                placeholder="First Name"
                required type="text" id="first_name"
                name="first_name" className="form-control"
              />
              <label htmlFor="name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={lastName}
                onChange={handleLastNameChange}
                placeholder="Last Name"
                required type="text" id="last_name"
                name="last_name" className="form-control"
              />
              <label>Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={employeeId}
                onChange={handleEmployeeIdChange}
                placeholder="EmployeeId"
                type="number" id="employeeId"
                name="employeeId" className="form-control"
              />
              <label>Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesPersonForm;
