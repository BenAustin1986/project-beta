import React, { useEffect, useState } from 'react';

function SaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeoples, setSalesPeoples] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesPeople] = useState('');
    const [customer, setCustomer] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const fetchAutomobile = async () => {
        try {
            const Url = 'http://localhost:8100/api/automobiles/';
            const response = await fetch(Url);

            if (response.ok) {
                const data = await response.json();
                const fautos = data.autos.filter(auto => auto.sold!=="sold")
                setAutomobiles(fautos);
            } else {
                throw new Error('Failed to fetch automobiles');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const fetchSalesPeople = async () => {
        try {
            const salespeopleUrl = "http://localhost:8090/api/salespeople/";
            const response = await fetch(salespeopleUrl);

            if (response.ok) {
                const data = await response.json();
                setSalesPeoples(data.salespeople);
            } else {
                throw new Error('Failed to fetch salespeople');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const fetchCustomer = async () => {
        try {
            const customerUrl = "http://localhost:8090/api/customers/";
            const response = await fetch(customerUrl);

            if (response.ok) {
                const data = await response.json();
                setCustomers(data.customer);
            } else {
                throw new Error('Failed to fetch customers');
            }
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            price: parseInt(price),
            automobile: automobile,
            salesperson: salesperson,
            customer: customer,
        };

        try {
            const salesUrl = "http://localhost:8090/api/sales/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                },
            };
            const response = await fetch(salesUrl, fetchConfig);

            if (response.ok) {
                setSuccessMessage('Sale created!');
                const autosUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`;
                const fetchSold = {
                    method: 'put',
                    body: JSON.stringify({ sold: true }),
                    headers: {
                        'Content-Type': 'application/json'
                    },
                };
                await fetch(autosUrl, fetchSold);
                fetchAutomobile();
                setPrice('');
                setAutomobile('');
                setSalesPeople('');
                setCustomer('');
            } else {
                throw new Error("Couldn't create sale");
            }
        } catch (error) {
            console.error("Error", error);
        }
    };

    const handlePrice = (event) => {
        const value = event.target.value;
        setPrice(value);
    };

    const handleAutomobile = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    };

    const handleSalesPeople = (event) => {
        const value = event.target.value;
        setSalesPeople(value);
    };

    const handleCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    };

    useEffect(() => {
        fetchAutomobile();
        fetchCustomer();
        fetchSalesPeople();
    }, []);

    return (
        <div>
            <h1>Create a Sale</h1>
            {successMessage && <p style={{ backgroundColor: 'red', color: 'white', padding: '10px', borderRadius: '5px', display: 'inline-block' }}>{successMessage}</p>}
            <form onSubmit={handleSubmit} id='create-sale-form'>
                <div className='div-tag'>
                    <label className='form-label'>VIN</label>
                    <select onChange={handleAutomobile} value={automobile} required id='automobile' name='automobiles' className='form-select'>
                        <option className='automobile'>Choose VIN</option>
                        {automobiles && automobiles.map(automobile => {
                            if (automobile.sold === false) {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                );
                            }
                        })}
                    </select>
                </div>
                <div>
                    <label className='form-label'>Sales People</label>
                    <select onChange={handleSalesPeople} required id='SalesPeople' name='salespeople' className='form-select'>
                        <option className='salespeople'>Choose Sales Person</option>
                        {salespeoples && salespeoples.map(salespeople => (
                            <option key={salespeople.id} value={salespeople.id}>
                                {salespeople.first_name} {salespeople.last_name}
                            </option>
                        ))}
                    </select>
                </div>
                <div>
                    <label className='form-label'>Customer</label>
                    <select onChange={handleCustomer} required id="Customer" name='customer' className='form-select'>
                        <option className='customer'>Choose customer</option>
                        {customers && customers.map(customer => (
                            <option key={customer.id} value={customer.id}>
                                {customer.first_name} {customer.last_name}
                            </option>
                        ))}
                    </select>
                </div>
                <div>
                    <input value={price} onChange={handlePrice} placeholder='price' name='price' />
                    <label className='form-label'>Price</label>
                </div>
                <button className='btn btn-primary'>Create</button>
            </form>
        </div>
    );
}

export default SaleForm;
