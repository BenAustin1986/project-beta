import { useState, useEffect } from 'react';

function ServiceHistory() {
	const [appointment, setAppointment] = useState([]);
	const [searchVin, setSearchVin] = useState('');

	useEffect(() => {
		const fetchURL = 'http://localhost:8080/api/appointments/';
		fetch(fetchURL)
			.then((response) => response.json())
			.then((data) => {
				setAppointment(data.appointment);
			});
	}, []);

	const handleSearch = (event) => {
		event.preventDefault();
		const filter = appointment.filter((i) => i.vin === searchVin);
		setAppointment(filter);
	};

	return (
		<>
			<h1 style={{ margin: '25px 0' }}>Service Appointments</h1>
			<form style={{ margin: '25px' }} onSubmit={handleSearch}>
				<div className="d-flex" style={{ width: '100%' }}>
					<label style={{ width: '100%', margin: 0 }}>
						<input
							style={{
								width: '100%',
								marginBottom: '0px',
								margin: '25px 0',
								textTransform: 'uppercase',
							}}
							className="form-control rounded-pill"
							type="text"
							maxLength={17}
							placeholder="Search by VIN #"
							value={searchVin}
							onChange={(event) => setSearchVin(event.target.value)}
						/>
					</label>
					<button
						className="btn btn-outline-secondary"
						type="submit"
						style={{
							marginLeft: '5px',
							marginTop: '24px',
							width: '50px',
							height: '40px',
							borderRadius: '38%',
						}}
					>
						🔍
					</button>
				</div>
			</form>
			<div
				style={{
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					width: '20%',
					margin: '0 auto',
					marginBottom: '25px',
				}}
			>
				<a
					href="/appointments/history"
					style={{
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						textDecoration: 'none',
					}}
				>
					<button className="form-control rounded-pill">Reset Search</button>
				</a>
			</div>

			<table className="table table-striped">
				<thead>
					<tr>
						<th>Vin</th>
						<th>is VIP?</th>
						<th>Customer</th>
						<th>Date/Time</th>
						<th>Technician</th>
						<th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					{appointment.map((i) => {
						return (
							<tr key={i.id}>
								<td>{i.vin}</td>
								<td>{i.vip ? 'Yes' : 'No'}</td>
								<td>{i.customer}</td>
								<td>
									{new Date(i.date_time).toLocaleString('en-US', {
										month: '2-digit',
										day: '2-digit',
										year: '2-digit',
										hour: 'numeric',
										minute: 'numeric',
										hour12: true,
									})}
								</td>
								<td>
									{i.technician.first_name} {i.technician.last_name}
								</td>
								<td>{i.reason}</td>
								<td>{i.status}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}
export default ServiceHistory;

