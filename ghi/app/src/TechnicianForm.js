import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const TechnicianForm = () => {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	// const [employeeId, setEmployeeId] = useState('')//set employee id to generate automatically.
	const navigate = useNavigate();
	const [successAlert, setSuccessAlert] = useState(false);

	const handleSubmit = (event) => {
		event.preventDefault();
		const newTech = {
			first_name: firstName,
			last_name: lastName,
		};

		const url = 'http://localhost:8080/api/technicians/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(newTech),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		fetch(url, fetchConfig)
			.then((response) => response.json())
			.then(() => {
				setSuccessAlert(true);
				const alertTimeout = setTimeout(() => {
					setSuccessAlert(false);
				}, 3000);
				const navigateTime = setTimeout(() => {
					navigate('/technicians');
				}, 1000);
				return () => {
					clearTimeout(alertTimeout);
					clearTimeout(navigateTime);
				};
			});
	};

	const handleFirstNameChange = (event) => {
		setFirstName(event.target.value);
	};

	const handleLastNameChange = (event) => {
		setLastName(event.target.value);
	};
	return (
		<div>
			{successAlert && (
				<div
					className="alert alert-success"
					role="alert"
					style={{
						position: 'fixed',
						top: '1rem',
						right: '1rem',
						zIndex: 1000,
						color: 'white',
						backgroundColor: 'green',
						borderColor: 'darkgreen',
						border: '1px solid',
						borderRadius: '5px',
						padding: '0.75rem 1.25rem',
					}}
				>
					Technician added!
				</div>
			)}
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1 style={{ marginTop: 0, marginBottom: '25px' }}>
							Add a new Technician
						</h1>
						<form onSubmit={handleSubmit} id="create-technician-form">
							<div className="form-floating mb-3">
								<input
									value={firstName}
									onChange={handleFirstNameChange}
									placeholder="First Name"
									required
									type="text"
									name="First Name"
									id="First Name"
									className="form-control placeholder"
								/>
								<label htmlFor="Name">First name</label>
							</div>

							<div className="form-floating mb-3">
								<input
									value={lastName}
									onChange={handleLastNameChange}
									placeholder="Last Name"
									required
									type="text"
									name="Last Name"
									id="Last Name"
									className="form-control placeholder"
								/>
								<label htmlFor="picture url">Last name</label>
							</div>
							<div style={{ margin: '25px 0' }}>
								<p>
									Employee ID will be automatically generated upon submission!
								</p>
							</div>
							<button className="btn btn-primary">Create</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};
export default TechnicianForm;
