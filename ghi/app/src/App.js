import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import VehicleModels from './VehicleModelsList';
import Nav from './Nav';
import Manufacturer from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import Automobile from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import ModelForm from './ModelForm';
import TechniciansList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';
import CustomerList from "./Sales/CustomerList";
import CustomerForm from "./Sales/CustomerForm";
import SalesPersonList from "./Sales/SalespersonList";
import SalesPersonForm from "./Sales/SalespersonForm";
import SaleList from "./Sales/SaleList";
import SaleForm from "./Sales/SaleForm";
import SalespersonHistory from "./Sales/SalespersonHistory";
import Footer from './Footer'; // Ensure this is correctly pointing to your Footer component


function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route index element={<MainPage />} />
					<Route path="automobiles">
						<Route path="" element={<Automobile />} />
						<Route path="create" element={<AutomobileForm />} />
					</Route>
					<Route path="manufacturers">
						<Route path="" element={<Manufacturer />} />
						<Route path="create" element={<ManufacturerForm />} />
					</Route>
					<Route path="models">
						<Route path="" element={<VehicleModels />} />
						<Route path="create" element={<ModelForm />} />
					</Route>
					<Route path="technicians">
						<Route path="" element={<TechniciansList />} />
						<Route path="create" element={<TechnicianForm />} />
					</Route>
					<Route path="appointments">
						<Route path="" element={<AppointmentList />} />
						<Route path="create" element={<AppointmentForm />} />
						<Route path="history" element={<ServiceHistory />} />
					</Route>

          <Route path="Customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>

          <Route path="SalesPerson">
            <Route index element={<SalesPersonList />} />
            <Route path="new" element={<SalesPersonForm />} />
          </Route>

          <Route path="Sale">
            <Route index element={<SaleList />} />
            <Route path="new" element={<SaleForm />} />
          </Route>

          <Route path="SalespersonHistory">
            <Route index element={<SalespersonHistory />} />
          </Route>

				</Routes>
			</div>
			<Footer />
		</BrowserRouter>
	);
}

export default App;
