import React, { useState, useEffect } from 'react';
import './MainPage.css';


function MainPage() {
  const [images, setImages] = useState([]);
  const [infoCards, setInfoCards] = useState([]);

  useEffect(() => {
    fetch('https://api.pexels.com/v1/search?query=cars', {
      headers: {
        'Authorization': '3N8kDxd2uEaBmV2Gnzq0CW3HF8xIUxVGxJ8aVjrubt9IyPUYmyGCwkVH'
      }
    })
      .then(response => response.json())
      .then(data => {
        const imageUrls = data.photos.map(photo => photo.src.original);
        setImages(imageUrls); // For the carousel

        // For infoCards, consider using the first four images from the fetched data
        const cardsData = data.photos.slice(0, 8).map((photo, index) => ({
          title: `Title ${index + 1}`, // Example title, adjust as needed
          description: `Description for card ${index + 1}`, // Example description, adjust as needed
          imageUrl: photo.src.original, // Use the original image URL from the API
        }));
        setInfoCards(cardsData);
      })
      .catch(error => console.error('Error fetching images:', error));
  }, []);



  return (
    <div className="main-page">
      {/* Carousel */}
      <div id="carouselExample" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
          {images.map((image, index) => (
            <div key={index} className={`carousel-item ${index === 0 ? 'active' : ''}`}>
              <img src={image} className="d-block w-100" alt={`Banner ${index}`} />
            </div>
          ))}
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>



      {/* Information Cards */}
      <div className="info-cards-container d-flex flex-wrap justify-content-around">
      {infoCards.map((card, index) => (
    <div key={index} className="col-6 col-sm-4 col-md-3 mb-4"> {/* Adjusted for 4 cards in a row on medium screens */}
      <div className="card rounded-lg h-100" style={{ overflow: 'hidden' }}> {/* Using h-100 for equal height */}
        <img src={card.imageUrl} className="card-img-top" alt={card.title} style={{ objectFit: 'cover', height: '140px' }} />
        <div className="card-body d-flex flex-column">
          <h5 className="card-title">{card.title}</h5>
          <p className="card-text">{card.description}</p>
          <button type="button" className="btn btn-primary mt-auto">Click Me</button>
            </div>
          </div>
        </div>
      ))}
    </div>


      {/* Schedule Service Appointment Section */}
      <section className="schedule-service py-5">
        <div className="container">
          <h2 className="text-center mb-4">Schedule a Service Appointment</h2>
          <form>
            <div className="form-group">
              <label htmlFor="name">Your Name</label>
              <input type="text" className="form-control" id="name" placeholder="Enter your name" />
            </div>
            <div className="form-group">
              <label htmlFor="email">Email address</label>
              <input type="email" className="form-control" id="email" placeholder="Enter your email" />
            </div>
            <div className="form-group">
              <label htmlFor="date">Preferred Date</label>
              <input type="date" className="form-control" id="date" />
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
          </form>
        </div>
      </section>
    </div>
  );
}

export default MainPage;
