import React, { useState } from 'react';

function CarTrimForm({ onSubmit }) {
  const [formState, setFormState] = useState({
    model: '',
    year: '',
    make: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(formState); // Pass form state up to parent component or handle it directly here
    // Optionally reset form state here if necessary
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="model">Model</label>
        <input
          type="text"
          className="form-control"
          id="model"
          name="model"
          value={formState.model}
          onChange={handleChange}
          placeholder="Enter model"
        />
      </div>
      <div className="form-group">
        <label htmlFor="year">Year</label>
        <input
          type="number"
          className="form-control"
          id="year"
          name="year"
          value={formState.year}
          onChange={handleChange}
          placeholder="Enter year"
        />
      </div>
      <div className="form-group">
        <label htmlFor="make">Make</label>
        <input
          type="text"
          className="form-control"
          id="make"
          name="make"
          value={formState.make}
          onChange={handleChange}
          placeholder="Enter make"
        />
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  );
}

export default CarTrimForm;
