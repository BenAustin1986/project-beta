import { useEffect, useState } from "react";
import { Link } from "react-router-dom";



function Automobile() {

    const handleDelete = async (auto) => {
        const autoUrl = `http://localhost:8100/api/automobiles/${auto.vin}`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(autoUrl, fetchConfig);
        if(response.ok) {
            fetchAutomobiles();
        }
    }

    const [autos, setAutos] = useState([]);

    const fetchAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchAutomobiles();
    }, []);

    return (
        <>
        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/automobiles/create" className="btn btn-primary btn-lg px-4 gap-3">Add an automobile</Link>
            </div>
        <table className="table table-striped">
        <thead>
            <tr className='white-text'>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
            </tr>
            </thead>
            <tbody>
            {autos.map((auto) => {
                return (
                <tr key={auto.id}>
                    <td>{auto.vin }</td>
                    <td>{auto.color }</td>
                    <td>{auto.year }</td>
                    <td>{auto.model.name }</td>
                    <td>{auto.model.manufacturer.name }</td>
                    <td>{auto.sold.toString()}</td>
                    <td>
                        <button onClick={() => handleDelete(auto)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
    )
}

export default Automobile;
