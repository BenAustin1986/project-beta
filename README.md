
# CarCar

CarCar is a application for managing dealerships. This new app is designed specifically for managing Inventory, Salepersons and Technicians. Built inside of the application is ROBUST functionality to help assist your team make life just a bit easier. Some of the key things the app handles are:
Service Appointments,
Technicians,
Salespeople,
Automobiles
this is just the a few things our newly updated application allows your team to do!

## Team: Zoom Zoom Autos

* Nate - Service
* Ben - Sales

## Design

![alt text](FinalDiagram.png)


# Getting Started
Im going to take you step by step on how to get started on your project:
GitLab
1. Sign in to your GitLab account.
2. Find your project you and your partner are working on
3. Fork the project(follow instructions after you click the fork button)
4. Once cloned click on the blue button that say Code and copy the HTTPS:REQUEST just as it is.
# TERMINAL
1. Open up a clean terminal
2. Type this command into your terminal ==> git clone https:(the copied code from GIT is what goes in her. )
3. CD into your directory (CD project name)
4. Once inside project type code . (this will open the VS Code)
# Docker
1. Once inside VS code open the internal terminal
2. then you want to start your containers follow these instructions:
    docker volume create two-shot-pgdata <== whatever your folder is
    docker-compose build
    docker-compose up (Make sure your signed in to docker you can do so in terminal by typing ==> docker login)
3. Your containers should be up and running
**Open up your React site: localhost:3000**

# GIT, COMMIT, PUSH changes
** Once you have your code running go to your README type in something and now we want to add those changes to our GitLab you can do so by following these instructions **
    git pull
... do some work
... test your work
    git add .
    git commit -m "added great stuff"
    git pull
    git push

## Service microservices
I had the responsibility on building the Service Microservice. That consist of me building out Service Appointments and TechniciaNS.
Technicians can be created, listed or deleted with built in functionality that automatically checks for employee id connected with Tech.
    - Add a technician
        -which prompts you to add First and Last name
        -Theres a functional Create button once clicked it takes you to a List of technicians previously added.
    - List all technicians
        -Has the fields Employeeid, First and Last name
        -A functional button to add a tech from List of Technicians

Service Appointments can be created and listed. once a form the filled out it service order i can be canceled or finish appointment. if cancelled it will be marked cancelled on the Sales history page along with finished if the service was completed. Your able to look up the vin with its specific number and a functional search button to only get the exact appointment info once vin is inputted.
    - Create Appointment
    - Show Service Appointments
    - Show service History

```
Technician
AutomobileVO
Appointment
```

# EndPoints
```
Action	Method	URL
List technicians	                GET	http://localhost:8080/api/technicians/
Create a technician	                POST	http://localhost:8080/api/technicians/
Delete a technician	                DELETE	http://localhost:8080/api/technicians/:id/
List appointments	                GET	http://localhost:8080/api/appointments/
Create an appointment	            POST	http://localhost:8080/api/appointments/
Delete an appointment	            DELETE	http://localhost:8080/api/appointments/:id/
appointment status to "canceled"	PUT	http://localhost:8080/api/appointments/:id/cancel/
appointment status to "finished"	PUT	http://localhost:8080/api/appointments/:id/finish/
```

# GET / Returns a List # Post / Creates # Put / Will update # Delete / Deletes

## Appointments Insomnia
```
curl --request DELETE \
  --url http://localhost:8080/api/appointments/:id \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.0' \
  --data '{
	"deleted": true
}
```
```
curl --request GET \
  --url http://localhost:8080/api/appointments/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.0' \
  --data '{
	"appointment": [
		{
			"vin": "1C3CC5FB2AN120174",
			"date_time": "2023-05-23T16:00:00+00:00",
			"technician": {
				"first_name": "Mo",
				"last_name": "Rahman",
				"employee_id": 1
			},
			"reason": "Oil Change",
			"status": "",
			"customer": "Maria Lopez",
			"id": 5,
			"vip": false
		},
	]
}
```
```
curl --request POST \
  --url http://localhost:8080/api/appointments/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.0' \
  --data '{
	"date_time": "2023-05-23 18:00",
	"reason": "Oil Change",
	"status": "created",
	"vin": "1C3CCBBG2EN115059",
	"customer": "John Smith",
	"technician": 5,

}
```
## Technicians Insomnia
```
curl --request GET \
  --url http://localhost:8080/api/technicians/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '
        {
	"technician": [
		{
			"first_name": "nathan",
			"last_name": "thomas",
			"employee_id": 1
		},
		{
			"first_name": "Kara",
			"last_name": "Thomas",
			"employee_id": 2
		},
		{
			"first_name": "Aaron ",
			"last_name": "Thomas",
			"employee_id": 3
		}
    ]
        }
```
```
curl --request POST \
  --url http://localhost:8080/api/technicians/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "first_name": "John",
  "last_name": "Smith"
}
```
```
curl --request DELETE \
  --url http://localhost:8080/api/technicians/:id/ \
  --header 'User-Agent: insomnia/8.6.1'
```

# Inventory

## Manufacturers/Insomnia
```
curl --request GET \
  --url http://localhost:8100/api/manufacturers/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}'
```
```
curl --request POST \
  --url http://localhost:8100/api/manufacturers/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "name": "Chrysler"
}'
```
```
curl --request DELETE \
  --url http://localhost:8100/api/manufacturers/:id/ \
  --header 'User-Agent: insomnia/8.6.1'
```

## Automobiles/insomnia
```
curl --request GET \
  --url http://localhost:8100/api/automobiles/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}
```
```
curl --request GET \
  --url http://localhost:8100/api/automobiles/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120164/",
			"id": 1,
			"color": "red",
			"year": 2023,
			"vin": "1C3CC5FB2AN120164",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Civic",
				"picture_url": ,
				"manufacturer": {
					"href": "/api/manufacturers/5/",
					"id": 5,
					"name": "Honda"
				}
			},
			"sold": false
		}
	]
}
```
```
curl --request POST \
  --url http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}'
```
```
curl --request PUT \
  --url http://localhost:8100/api/automobiles/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "color": "red",
  "year": 2012,
  "sold": true
}
```
```
curl --request DELETE \
  --url http://localhost:8100/api/automobiles/:vin/ \
  --header 'User-Agent: insomnia/8.6.1'
```
  ## Vehicle Models
  ```
  curl --request GET \
  --url http://localhost:8100/api/models/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```
```
curl --request POST \
  --url http://localhost:8100/api/models/1/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "name": "Sebring",
  "picture_url": "",
  "manufacturer_id": 1
}'
```
```
curl --request GET \
  --url http://localhost:8100/api/models/:id/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}'
```
```
curl --request PUT \
  --url http://localhost:8100/api/models/:id/ \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/8.6.1' \
  --data '{
  "name": "Sebring",
  "picture_url": ""
}
```
```
curl --request DELETE \
  --url http://localhost:8100/api/models/:id/ \
  --header 'User-Agent: insomnia/8.6.1'
```

## Service microservice
### Models in the Sales Microservice:
- AutomobileVO: This is a value object representing automobile data. It retrieves information about automobiles in the inventory. The data is obtained through a poller that automatically fetches updated information from the Inventory Microservice.
- Customer: This represents the customer data. It contains their first and last names, address, and phone number.
- SalesPerson: This represents salesperson data. It contains their first and last names, and their employee id.
- Sale: This model interacts with the other three models. It is responsible for recording sales and requires data from AutomobileVO, Customer, and SalesPerson to create complete sales records.
## Sales microservice
### Integration with the Inventory Microservice:
- The Sales Microservice integrates with the Inventory Microservice because the information about the automobiles being sold resides in the inventory. When recording a new sale, the Sales Microservice needs to access information about the cars available in the inventory, so that it knows which ones have been sold and which are available to be sold. This integration allows the Sales Microservice to fetch real-time data about available automobiles from the Inventory Microservice, ensuring that the sales records are accurate and up to date.
## Accessing Endpoints to Send and View Data - Access through Insomnia:
### Customers:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Show a specific customer | GET | http://localhost:8090/api/customers/id/
To create a Customer (SEND THIS JSON BODY):
```
{
    "first_name": "Bob",
    "last_name": "Weadababyeetsaboy",
    "address": "123 Sycamore Ln",
    "phone_number": "1234567890"
}
```
Return Value of Creating a Customer:
```
{
    "first_name": "Bob",
    "last_name": "Weadababyeetsaboy",
    "address": "123 Sycamore Ln",
    "phone_number": "1234567890",
    "id": 14
}
```
Return value of Listing all Customers:
```
{
    "customer": [
        {
            "first_name": "Bob",
            "last_name": "Weadababyeetsaboy",
            "address": "123 Sycamore Ln",
            "phone_number": "123-456-7890",
            "id": 5
        },
        {
            "first_name": "Fred",
            "last_name": "Fred",
            "address": "123 Fred St",
            "phone_number": "12345657890",
            "id": 6
        },
        {
            "first_name": "a",
            "last_name": "a",
            "address": "asdf",
            "phone_number": "1234567890",
            "id": 7
        },
        {
            "first_name": "Bob",
            "last_name": "Robert",
            "address": "123 Sycamore",
            "phone_number": "1234567890",
            "id": 10
        },
        {
            "first_name": "a",
            "last_name": "a",
            "address": "a",
            "phone_number": "12",
            "id": 13
        },
        {
            "first_name": "Bob",
            "last_name": "Weadababyeetsaboy",
            "address": "123 Sycamore Ln",
            "phone_number": "1234567890",
            "id": 14
        }
    ]
}
```
### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Salesperson details | GET | http://localhost:8090/api/salesperson/id/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/
To create a salesperson (SEND THIS JSON BODY):
```
{
    "first_name": "asdfJack",
    "last_name": "Abcdes",
    "employee_id": 12349
}
```
-(The employee ID field MUST be unique and less than or equal to 5 digits long. If you try it and it fails, try changing the ID to another random number and try again.)
Return Value of creating a salesperson:
```
    "first_name": "asdfJack",
    "last_name": "Abcdes",
    "employee_id": 12349,
    "id": 30
}
```
List all salespeople Return Value:
```
{
    "salespeople": [
        {
            "first_name": "Bob",
            "last_name": "Evans",
            "employee_id": 12345,
            "id": 1
        },
        {
            "first_name": "Bob",
            "last_name": "Evan",
            "employee_id": 12344,
            "id": 4
        },
        {
            "first_name": "Jack",
            "last_name": "Abcdefgpqrstuvwxyz",
            "employee_id": 1119,
            "id": 6
        },
        {
            "first_name": "asdfJack",
            "last_name": "Abcdes",
            "employee_id": 12,
            "id": 10
        },
        {
            "first_name": "Jack",
            "last_name": "Abcdes",
            "employee_id": 1,
            "id": 8
        },
        {
            "first_name": "Aakash",
            "last_name": "Tilapia",
            "employee_id": 69,
            "id": 24
        },
        {
            "first_name": "a",
            "last_name": "aa",
            "employee_id": 777,
            "id": 25
        },
        {
            "first_name": "Ben",
            "last_name": "Austin",
            "employee_id": 21,
            "id": 27
        },
        {
            "first_name": "a",
            "last_name": "aa",
            "employee_id": 16,
            "id": 28
        },
        {
            "first_name": "asdfJack",
            "last_name": "Abcdes",
            "employee_id": 12349,
            "id": 30
        }
    ]
}
```
### Sales:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/salesrecords/
| Create a new sale | POST | http://localhost:8090/api/salesrecords/
```
Create a New Sale (SEND THIS JSON BODY):
```
{
    "price": "9999",
    "salesperson": 1,
    "customer": 7,
    "automobile": "1873896"
}
```
Return Value of Creating a New Sale:
```
{
    "automobile": {
        "vin": "1873896",
        "sold": false,
        "id": 16
    },
    "id": 27,
    "price": "9999",
    "customer": {
        "first_name": "a",
        "last_name": "a",
        "address": "asdf",
        "phone_number": "1234567890",
        "id": 7
    },
    "salesperson": {
        "first_name": "Bob",
        "last_name": "Evans",
        "employee_id": 12345,
        "id": 1
    }
}
```
